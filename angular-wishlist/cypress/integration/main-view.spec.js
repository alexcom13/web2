const { createYield } = require("typescript");

/// <reference types = "cypress" />

describe('ventana principal', () => {

    const destino = 'Mexico';
    const url = 'https://VivaMexico.org';

    beforeEach(() => {
        cy.visit('http://localhost:4200');
    })

    it('tiene encabezado correcto y en español por defecto', () => {
        cy.contains('angular-whishlist');
        cy.get('h1 b').should('contain', 'HOLA es');
      });

    it('añadir destino ', () => {

      cy.get('#nombre').click()
      .type(destino);

      cy.get('#imagenUrl').click()
      .type(url);

      cy.get('button').click();
    });
    
    it('ir a ver mas ', () => {

      cy.get('a').contains('Ver!').click();
    })
  });