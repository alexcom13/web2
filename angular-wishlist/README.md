# AngularWishlist

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.0.2.

Mis notas :):

----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

<Comandos útiles de consola>

# Ativar modo observador de consola: tsc -w 

# Crea el archivo de configuracion de Typescrpt (tsconfig.json): tsc --init 

----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

<Conceptos Typescprit, Javascprit and ES6> 

## Diferencia entre var y let  

let te define una variable para un scoope (de llave a llave) y el var te lo define "globalmente"

## Excluir archivos del observador (tsc -w)

Configurar en el json el exclude con el nombre de la carpeta que quieres excluir ("exluce" = nombre de la carpeta)

## Templates literales

Se ocupan los backticks (``) y para concatenar una varible es con ${ varible } dentro de los corchetes es code puro de Javascript,dentro de los blackticks se toma literal todo hasta los saltos de linea y espacios. \n es un salto de linea

## Argumentos opcionales y defecto funciones 

Se indican con el signo ? el argumento opcional y para el por defecto se incializa en el argumento. Ejs: fuction(nombre?: string, apodo: string = "Chuy"){}

## Arrow fuctions 

let fuction_name = (argumento: tipo_de_dato) => lo_que_quieras_que_haga; ó
let fuction_name = (argumento: tipo_de_dato) => { lo_que_quieras_que_haga };
Tmb no modifica el this.

## enum 

enum nombre {
    value1,
    value2,
    ...,
    valueN
}; con N natural number

## type assertions

Esto se hace despues de declarar la varibale de tipo any, y hay dos formas: ejs: let message; ,forma 1: (<tipo_de_variable>message) ,forma 2; (message as tipo_de_variable). Awb se usan los paréntesis

## interfaces 

interface nombre {
    declaration1,
    declaration2,
    ...,
    declarationN
}; 
con N natural number, tmb puede incluir funciones que tomen argumentos vacios y returnen void

## class 

las clases pueden tener declaraciones y funciones con argumentos no vacios y returnar cosas, ej:

class PointR2 {
    x: number; //componente x
    y: number: //componente y 

    NormalizarPoint = function() => 
}

----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

<Conceptos Angular>

## Uso de node.js para apis 

ir al root del proyecto y ver el gitignore para poner: node_modules/ en  # dependencies y quitar
node_modules/. Luego
npm init (en la carpeta donde va a estar la api, create a package.json file)

npm express (dependencias )

npm corse ( para comunicar entre browser y que no nos rechaze la comunicaciones de otros puertos)

node app.js (para iniciarlo (en lugar de app vendria el nombre que dimos en la npm init))

## Generar un guard:

ng g guard el_nombre (los guards tan mamones porque son tipo seguridad de validacion)

Para ello ocupamos CanActivate (ver en usuario-logueado.guard.ts)

## Generar un module routing:

ng g m el_nombre --module app --flat --routing (Te crea un nuevo Module (hijo del raiz) y puedes hacer components dentro, como en /reservas)

## Crear un angular generate directive 

ng g d nombre

## Unit testing Jasmin 

ng test (correrá todas los specs)

## End to end (cypress) 
<https://docs.cypress.io/guides/overview/why-cypress.html#In-a-nutshell>

npm install cypress --save -dev

npm run e2e (iniciar test)

funciones clave: get('Dom object by selector or alias.'), contains('Dom object containing the text'), type('algo que typear'), click(), 
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

<Understanding the weird parts of Javascript>

# Syntax parser

Es un programa que lee tu code y checa la sintaxis (parser es analizar)

# lexical environment 

En donde esta fisicamente algo en el code que armaste. (Esto existe en lenguajes donde importa donde escribes el codigo, o sea, como estructuras el roden en que escribes codigo)

# execution context 

Es como una tipo funcion que envuelve codigo para poder gestionarlo mientra se ejecuta

# Name\value pair 

un nombre que mapea un unico valor. Puede definirse un nombre mas de una vez pero solo puede tener un valor en un contexto dado

# object

una coleccion de name\valuse pairs 


