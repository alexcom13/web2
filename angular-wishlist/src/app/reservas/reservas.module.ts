import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReservasRoutingModule } from './reservas-routing.module';
import { ReservasListadComponent } from './reservas-listad/reservas-listad.component';
import { ReservasDetalleComponent } from './reservas-detalle/reservas-detalle.component';
import { ReservasApiClientService } from './reservas-api-clinet.service';

@NgModule({
  imports: [
    CommonModule,
    ReservasRoutingModule
  ],
  providers: [
    ReservasApiClientService
  ],
  declarations: [ReservasListadComponent, ReservasDetalleComponent]
})
export class ReservasModule { }
