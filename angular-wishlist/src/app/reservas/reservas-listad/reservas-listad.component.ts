import { Component, OnInit } from '@angular/core';
import { ReservasApiClientService } from '../reservas-api-clinet.service';

@Component({
  selector: 'app-reservas-listad',
  templateUrl: './reservas-listad.component.html',
  styleUrls: ['./reservas-listad.component.css']
})
export class ReservasListadComponent implements OnInit {

  constructor(public api: ReservasApiClientService) { }

  ngOnInit(): void {
  }

}
