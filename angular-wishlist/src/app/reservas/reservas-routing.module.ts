import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReservasListadComponent } from './reservas-listad/reservas-listad.component';
import { ReservasDetalleComponent } from './reservas-detalle/reservas-detalle.component';

const routes: Routes = [
  { path: 'reservas',  component: ReservasListadComponent },
  { path: 'reservas/:id',  component: ReservasDetalleComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReservasRoutingModule { }
